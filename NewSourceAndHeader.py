import sublime
import sublime_plugin
import functools

#
# see also: https://www.sublimetext.com/docs/3/api_reference.html
#

#
# asks for name then creates template header and source file from that name
# works with a side bar menu file to be available with right-click 
#
class NewSourceAndHeaderCommand(sublime_plugin.WindowCommand):

    # need a default 'is_visible()' member - this dictates when this command appears in the context menu
    def is_visible(self, dirs):
        return len(dirs) == 1

    def run(self, dirs):
        self.window.show_input_panel("Enter name of module:", "", functools.partial(self.on_done, dirs[0]), None, None)
        # show_input_panel() doesn't block, so create the files in the on_done() function.

    # called when user closes the input panel (hits return)
    # function is modified via functools to take an extra directory as an argument
    def on_done(self, parent_dir, text):
        # argument 'text' is the text entered in the input panel
        sublime.status_message("New modules are: " + text + ".c and " + text + ".h")

        header_file = self.window.new_file() # new_file() returns a *View*
        source_file = self.window.new_file()

        header_file.set_syntax_file('Packages/C++/C.tmLanguage')
        source_file.set_syntax_file('Packages/C++/C.tmLanguage')

        header_file.set_name(text + ".h")
        source_file.set_name(text + ".c")

        header_template = """/**
    @file
    Header description

    @https{{confluence.gentex.com/pages/viewpage.action?title=Home&spaceKey=~derek.bray}}
*/
#ifndef {0}
#define {0}

#include "GNTX_Common.h"


#endif  /*  {0}  */
""".format(text.upper() + "_H")
        header_file.run_command("insert_snippet", {"contents": header_template})


        source_template = """/**
    @file
    Source description

    @https{{confluence.gentex.com/pages/viewpage.action?title=Home&spaceKey=~derek.bray}}
*/
#include "{0}.h"


""".format(text)
        source_file.run_command("insert_snippet", {"contents": source_template})


        source_file.settings().set('default_dir', parent_dir)
        header_file.settings().set('default_dir', parent_dir)

        # let me test something: comment out the save calls here, let user save on build or similar
        header_file.run_command('save');  #  not ideal, it brings up the save dialog. but i can dismiss that with return key, so it's not too bad
        source_file.run_command('save');



# here's a command for reference:
# class NewFileAtCommand(sublime_plugin.WindowCommand):
#     def run(self, dirs):
#         v = self.window.new_file()

#         if len(dirs) == 1:
#             v.settings().set('default_dir', dirs[0])

#     def is_visible(self, dirs):
#         return len(dirs) == 1
