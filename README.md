# README #

A collection of snippets, extensions, syntax highlighters, etc. for Sublime Text 3 text editor for use at Gentex.


### How do I get set up? ###

* Pull this repo down to Sublime Text 3's Packages folder. Next to the 'User' folder is a good spot. Sublime should detect it and incorporate the collection automatically. Worst case, you'll need to re-start Sublime Text.
	* To find Sublime Text 3's Packages folder:
		* in Windows: in the main menu, Preferences -> Browse Packages
		* Some basic package reference information: http://www.sublimetext.com/docs/3/packages.html
* Configuration: none, wysiwyg.
* Dependencies: none, the collection is self-contained.

### Talk to someone ###

* Questions? Bug fix? Feature idea? Contact: derek.bray@gentex.com

